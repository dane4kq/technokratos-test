from rest_framework import serializers

from api.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "name", "email")
        read_only_fields = ("id", "email")


class UserCreateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = self.Meta.model.objects.create_user(**validated_data)
        user.password = None
        return user

    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'password')
        read_only_fields = ['id', ]
