from django.urls import re_path, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers
from rest_framework.authtoken import views as auth_views

from api.views import UserModelViewSet, NoteModelViewSet

schema_view = get_schema_view(
    openapi.Info(
        title="Referal API",
        default_version="v1",
        description="Technokratos API",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="zanzydnd@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
)

user_router = routers.SimpleRouter()
user_router.register(r"user", UserModelViewSet, basename="User")

notes_router = routers.SimpleRouter()
notes_router.register(r"note", NoteModelViewSet, basename="Note")

urlpatterns = [
    path("auth/", auth_views.obtain_auth_token),
    *user_router.urls,
    *notes_router.urls,
    re_path("swagger(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
