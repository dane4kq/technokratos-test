import json

from rest_framework import serializers

from api.models import Note, Tag, User
from api.serializers.user import UserSerializer


class NoteSerializer(serializers.ModelSerializer):
    tags = serializers.SlugRelatedField(
        many=True,
        slug_field='name',
        queryset=Tag.objects.all(),
        required=False
    )
    user = UserSerializer(read_only=True, many=False)

    def create(self, validated_data, **kwargs):
        tags = validated_data.pop('tags')
        note = Note.objects.create(**validated_data, user_id=kwargs.get('user_id'))
        note.tags.add(*tags)

        self.fake_data = dict(validated_data)
        tags_name = []
        for i in tags:
            tags_name.append(i.name)
        self.fake_data['tags'] = tags_name
        self.fake_data['id'] = note.id
        return note

    class Meta:
        model = Note
        fields = ("id", "name", "text", "tags", "user")
        read_only_fields = ("id",)
