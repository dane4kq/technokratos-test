from django.db import models
from django.utils.text import slugify

from api.models.tag import Tag
from api.models.user import User


class Note(models.Model):
    name = models.CharField(max_length=100, unique=True)

    text = models.TextField()
    created_at = models.DateField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notes")
    tags = models.ManyToManyField(Tag, related_name="notes")

    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Note, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
