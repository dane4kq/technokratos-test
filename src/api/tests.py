import json
from pprint import pprint

from django.test import TestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory, force_authenticate, APITestCase
from api.models import User, Tag, Note
from api.views import UserModelViewSet
from django.test import Client


class TestCaseForUser(APITestCase):
    def setUp(self) -> None:
        user1 = User.objects.create(email='test1@mail.ru', name='name1', password='123')
        Token.objects.create(user=user1)
        user2 = User.objects.create(email='test2@mail.ru', name='name2', password='123')
        Token.objects.create(user=user2)
        user3 = User.objects.create(email='test3@mail.ru', name='name3', password='123')
        Token.objects.create(user=user3)

    def test_get_user_info(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)
        response = self.client.get("/api/v1/user/")
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).get("/api/v1/user/")
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, {'id': user.id, 'name': 'name1', 'email': 'test1@mail.ru'})

    def test_update_user(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        update_data = json.dumps({"name": "name1_altered"})

        response = self.client.put(f"/api/v1/user/{user.id}/", update_data, content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).put(f"/api/v1/user/{user.id}/",
                                                                            update_data,
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data, {'id': user.id, 'name': 'name1_altered', 'email': 'test1@mail.ru'})

    def test_delete_user(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        response = self.client.delete(f"/api/v1/user/{user.id}/")
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).delete(f"/api/v1/user/{user.id}/")
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_create_user(self):
        insert_failure_data = json.dumps({"email": "test1@mail.ru", "name": "name1", "password": "123"})
        insert_data = json.dumps({"email": "test4@mail.ru", "name": "name4", "password": "123"})
        response = self.client.post(f"/api/v1/user/", insert_failure_data,
                                    content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post(f"/api/v1/user/", insert_data,
                                    content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

        auth_data = json.dumps({"username": "test4@mail.ru", "password": "123"})
        response = self.client.post("/api/v1/auth/", auth_data, content_type='application/json')

        self.assertEquals(response.status_code, status.HTTP_200_OK)


class TestCaseForNote(APITestCase):
    def setUp(self) -> None:
        user1 = User.objects.create(id=1, email='test1@mail.ru', name='name1', password='123')
        Token.objects.create(user=user1)
        user2 = User.objects.create(id=2, email='test2@mail.ru', name='name2', password='123')
        Token.objects.create(user=user2)

        tag1 = Tag.objects.create(id=1, name="tag1")
        tag2 = Tag.objects.create(id=2, name="tag2")
        tag3 = Tag.objects.create(id=3, name="tag3")

        note1 = Note.objects.create(id=100, name="Note_1", text="test", user=user1)
        note1.tags.add(tag1, tag3)

        note1 = Note.objects.create(id=200, name="Note_2", text="test", user=user2)
        note1.tags.add(tag2)

    def test_get_list_notes(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        response = self.client.get(f"/api/v1/note/")
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).get("/api/v1/note/",
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertEquals(response.json(), {'count': 1, 'next': None, 'previous': None, 'results': [
            {'id': 100, 'name': 'Note_1', 'text': 'test', 'tags': ['tag1', 'tag3'],
             'user': {'id': 1, 'name': 'name1', 'email': 'test1@mail.ru'}}]})

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).get("/api/v1/note/?q=1",
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertEquals(response.json(), {'count': 1, 'next': None, 'previous': None, 'results': [
            {'id': 100, 'name': 'Note_1', 'text': 'test', 'tags': ['tag1', 'tag3'],
             'user': {'id': 1, 'name': 'name1', 'email': 'test1@mail.ru'}}]})

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).get("/api/v1/note/?tags=tag3,tag1",
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertEquals(response.json(), {'count': 1, 'next': None, 'previous': None, 'results': [
            {'id': 100, 'name': 'Note_1', 'text': 'test', 'tags': ['tag1', 'tag3'],
             'user': {'id': 1, 'name': 'name1', 'email': 'test1@mail.ru'}}]})

    def test_get_single_note(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        response = self.client.get(f"/api/v1/note/1/")
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).get("/api/v1/note/100/",
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

        self.assertEquals(response.json(), {
            'id': 100, 'name': 'Note_1', 'text': 'test', 'tags': ['tag1', 'tag3'],
            'user': {'id': 1, 'name': 'name1', 'email': 'test1@mail.ru'}})

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).get("/api/v1/note/200/",
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_note(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        update_data = json.dumps({'name': 'Note_1_altered', 'text': 'test2', 'tags': ['tag2']})

        response = self.client.put("/api/v1/note/1/", update_data, content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).put(f"/api/v1/note/100/",
                                                                            update_data,
                                                                            content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.json(), {'id': 100, 'name': 'Note_1_altered', 'text': 'test2', 'tags': ['tag2'],
                                            'user': {'id': 1, 'name': 'name1', 'email': 'test1@mail.ru'}})

    def test_create_note(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        insert_failure_data = json.dumps({
            "name": "Note_2",
            "text": "string",
            "tags": [
                "str"
            ]
        })
        insert_data = json.dumps({
            "name": "Note_3",
            "text": "string",
            "tags": [
                "tag1", "tag2", "tag3"
            ]
        })

        response = self.client.post(f"/api/v1/note/", insert_failure_data,
                                    content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).post(f"/api/v1/note/", insert_failure_data,
                                                                             content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).post(f"/api/v1/note/", insert_data,
                                                                             content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_delete_note(self):
        user = User.objects.get(email='test1@mail.ru')
        user_token = Token.objects.get(user=user)

        response = self.client.delete("/api/v1/note/100/", content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = Client(HTTP_AUTHORIZATION='Token ' + user_token.key).delete(f"/api/v1/note/100/",
                                                                               content_type='application/json')
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)
