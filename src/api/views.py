from django.db.models import Q
from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import BasePermission
from rest_framework.response import Response

from api.models import User, Note
from api.serializers.note import NoteSerializer
from api.serializers.user import UserSerializer, UserCreateSerializer


class UserEndpointsPermissions(BasePermission):
    USER_METHODS = ["GET"]

    def has_object_permission(self, request, view, obj):
        if not request.auth:
            return False

        if request.method in self.USER_METHODS:
            return True

        user = Token.objects.get(key=request.auth).user
        return obj.id == user.id


class UserModelViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = (UserEndpointsPermissions,)

    def list(self, request, *args, **kwargs):
        if not request.auth:
            raise PermissionDenied(detail="Ауттенфицируйтесь.")

        token = Token.objects.get(key=request.auth)

        user = User.objects.get(id=token.user_id)

        serializer = self.get_serializer(user, many=False)
        return Response(serializer.data)

    def get_serializer_class(self):
        if self.action == 'create':
            return UserCreateSerializer
        return UserSerializer


class NotesEndpointsPermissions(BasePermission):
    def has_permission(self, request, view):
        if not request.auth:
            return False
        return True

    def has_object_permission(self, request, view, obj):
        if not request.auth:
            return False
        user = Token.objects.get(key=request.auth).user
        request.user = user

        return obj.user_id == user.id


class NoteModelViewSet(viewsets.ModelViewSet):
    serializer_class = NoteSerializer
    permission_classes = (NotesEndpointsPermissions,)

    def get_object(self):
        note = Note.objects.get(id=self.kwargs.get('pk'))
        self.check_object_permissions(self.request, note)
        return note

    def get_queryset(self):
        q = self.request.query_params.get('q')
        tags = self.request.query_params.get('tags')
        filtering = Q(user=self.request.user)

        if tags:
            filtering &= Q(tags__name__in=tags.split(','))

        if q:
            filtering &= Q(name__contains=q) | Q(text__contains=q)

        return Note.objects.filter(filtering).select_related(
            'user').prefetch_related('tags').distinct()

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.create(serializer.validated_data, user_id=request.user.id)

        return Response(status=201, data=serializer.fake_data)
