FROM python:3.9

ENV PYTHONUNBUFFERED 1

WORKDIR /technokratos

COPY requirements.txt ./requirements_technokratos.txt

RUN pip install -r requirements_technokratos.txt

COPY . .

CMD python3 src/manage.py migrate  && gunicorn technokratos.wsgi --chdir src --bind 0.0.0.0 --preload --log-file -

