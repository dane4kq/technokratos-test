# Описание #

Тестовое задание для технократии.

1. Необходимо прописать .env , .env.docker - файлы в src; Там есть пример .env.example


## Тестирование ##

- `python3 -m venv env` 
- `source env/bin/activate`
- `python pip install -r requirements.txt`

Запуск тестов

- `python src/manage.py test api`


## Развёртывание приложения ##

Можно поднять через compose:
!!не забыть прописать .env.docker 

   - `docker-compose up --build -d`
