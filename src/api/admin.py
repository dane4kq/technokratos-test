from django.contrib import admin

# Register your models here.
from api.models import Tag, User

admin.site.register(Tag)
admin.site.register(User)
