import factory
from .models import Note, User, Tag


class TagFactory(factory.DjangoModelFactory):
    class Meta:
        model = Tag


class NoteFactory(factory.DjangoModelFactory):
    class Meta:
        model = Note
